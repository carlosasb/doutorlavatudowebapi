﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoutorLavaTudoWebAPI.Models
{
    public class Agendamentos
    {
        public int ID { get; set; }
        public string Begin { get; set; }
        public string End { get; set; }
        public string Date { get; set; }
        public int TecnicoID { get; set; }
    }
}