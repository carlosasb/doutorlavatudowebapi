﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DoutorLavaTudoWebAPI.Models
{
    public class Servicos
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Price { get; set; }
        public string Duration { get; set; }
    }
}