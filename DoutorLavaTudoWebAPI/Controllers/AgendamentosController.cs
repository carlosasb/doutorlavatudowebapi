﻿using DoutorLavaTudoWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DoutorLavaTudoWebAPI.Controllers
{
    public class AgendamentosController : ApiController
    {
        // stores all Agendamentos in a list
        List<Agendamentos> AgendamentosList = new List<Agendamentos>()
        {
            new Agendamentos { ID = 1, Begin = "09:00", End = "09:35", Date = "04-05-2017", TecnicoID = 1},
            new Agendamentos { ID = 2, Begin = "09:00", End = "09:55", Date = "04-05-2017", TecnicoID = 2},
            new Agendamentos { ID = 3, Begin = "10:25", End = "10:37", Date = "04-05-2017", TecnicoID = 1},
            new Agendamentos { ID = 4, Begin = "11:25", End = "11:55", Date = "04-05-2017", TecnicoID = 2}
        };

        public IEnumerable<Agendamentos> GetAllAgendamentos()
        {
            return AgendamentosList;
        }

        [Route("api/tecnicos/{TecnicoID}/agendamentos/{Date}")]
        public IHttpActionResult GetAgendamentoByDate(int TecnicoID, string Date)
        {
            // AgendamentosDoDia will hold all Agendamentos objects that match the Date and TecnicoID; is empty is no match is found
            List<Agendamentos> AgendamentosDoDia = new List<Agendamentos>();

            foreach (var agendamento in AgendamentosList)
            {
                if (agendamento.Date == Date && agendamento.TecnicoID == TecnicoID)
                {
                    AgendamentosDoDia.Add(agendamento);
                }
            }

            if (AgendamentosDoDia == null)
            {
                return NotFound();
            }
            else { return Ok(AgendamentosDoDia); }
        }

        [Route("api/tecnicos/{TecnicoID}/agendamentos")]
        public IHttpActionResult GetAgendamentoByDate(int TecnicoID)
        {
            // AgendamentosDoDia will hold all Agendamentos objects that match the Date and TecnicoID; is empty is no match is found
            List<Agendamentos> AgendamentosDoDia = new List<Agendamentos>();

            foreach (var agendamento in AgendamentosList)
            {
                if (agendamento.Date == DateTime.Now.Date.ToString() && agendamento.TecnicoID == TecnicoID)
                {
                    AgendamentosDoDia.Add(agendamento);
                }
            }

            if (AgendamentosDoDia == null)
            {
                return NotFound();
            }
            else { return Ok(AgendamentosDoDia); }
        }

        [HttpDelete]
        public HttpResponseMessage DeleteAgendamento(int ID)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            var agendamento = AgendamentosList.FirstOrDefault((p) => p.ID == ID);
            
            if (agendamento == null)
            {
                response.Content = new StringContent("Nenhum agendamento encontrado.");
                return response;
            }
            else
            {
                if (AgendamentosList.Remove(agendamento))
                {
                    response.Content = new StringContent("Agendamento removido.");
                    return response;
                }else
                {
                    response.Content = new StringContent("Agendamento não pode ser removido.");
                    return response;
                }
            }
        }
    }
}