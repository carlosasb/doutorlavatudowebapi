﻿using DoutorLavaTudoWebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace DoutorLavaTudoWebAPI.Controllers
{
    public class TecnicosController : ApiController
    {
        // stores all Tecnicos ia a list
        List<Tecnicos> TecnicosList = new List<Tecnicos>()
        {
            new Tecnicos { ID = 1, Name = "João Batista da Cunha"},
            new Tecnicos { ID = 2, Name = "Jacqueline Dias"}
        };

        public IEnumerable<Tecnicos> GetAllTecnicos()
        {
            return TecnicosList;
        }

        public IHttpActionResult GetTecnico(int ID)
        {
            var tecnico = TecnicosList.FirstOrDefault((p) => p.ID == ID);
            if (tecnico == null)
            {
                return NotFound();
            }
            else { return Ok(tecnico); }
        }
    }
}
