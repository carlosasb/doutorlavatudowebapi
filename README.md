# README #

### What is this repository for? ###

As respostas para o TESTE C# da empresa Doutor LavaTudo podem ser encontrados aqui.

### How do I get set up? ###

As respostas da parte 1 estão no arquivo respostas.html presente na pasta /teste\_DoutorLavaTudo. 
O modelo gráfico da base de dados esta na pasta /teste\_DoutorLavaTudo/img.

A parte 2 exige o uso do Visual Studio Community 2015 (nenhum teste foi feito para versões anteriores)
e usa a ASP .Net MVC WebAPI templates. Os controllers derivam de APIcontroller, necessitando dos templates 
web do Visual C#. 

Para rodar, apenas rode o projeto de dentro do Visual Studio Community 2015 e adicione na URL de seu browser padrão,
após localhost{numero}, /api e o caminho que quiser. 

No momento apenas /tecnicos, /tecnicos/{id}, /agendamentose /tecnicos/{tecnicos\_id}/agendamentos/{data} funcionam.
Nota sobre a data: devido a um pequeno problema de formatação que não tive tempo de resolver, a data tem que ser 
colocada e a data atual não é o padrão.

### Who do I talk to? ###

Entre em contato em:
CASBraga@gmail.com